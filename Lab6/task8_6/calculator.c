#include <stdio.h>
#include <stdlib.h>

float eval(void);

int main()
{
    puts("Enter a mathematical expression (using only \"0-9, +, -, *, / , (, )\")");
    puts("Press \"=\" at the end.\n");

    printf("\nCalculated result: %.1f\n", eval());

    return 0;
}

float eval(void)
{
    char operation, next;
    float answer = 0, number;

    operation = '+';

    while ((operation != '=') && (operation != ')'))
    {
        next = getchar();

        if (next == '(')
            number = eval();

        else
        {
            ungetc(next, stdin);
            scanf("%f", &number);
        }

        switch (operation)
        {
        case '+':
            answer += number; break;
        case '-':
            answer -= number; break;
        case '*':
            answer *= number; break;
        case '/':
            answer /= number;
            if (number == 0)
            {
                puts("\nDivision by zero is not allowed.");
                exit(1);
            }
            break;
        }

        operation = getchar();
    }

    return answer;
}
