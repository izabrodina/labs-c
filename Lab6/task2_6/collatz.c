#include <stdio.h>

#define MIN 2
#define MAX 1000000

unsigned int evenOrOdd(unsigned int);

int main()
{
    int maxSequence = 0, count;
    unsigned int currentNumber, number;

    for (currentNumber = MIN; currentNumber <= MAX; currentNumber++)
    {
        count = evenOrOdd(currentNumber);
        //printf("%d - %d\n", currentNumber, count);

        if (count > maxSequence)
        {
            maxSequence = count;
            number = currentNumber;
        }
    }

    printf("\nThe longest sequence of numbers starts"
           " with %d and consists of %d integers.\n",
           number, maxSequence);

    return 0;
}

unsigned int evenOrOdd(unsigned int number)
{
    unsigned static int count = 1; // because we already have the first number
    int step;

    if (number == 1)
    {
        step = count;
        count = 1;
        return step;
    }

    else if (number % 2 == 0)
        number /= 2;

    else
        number = number * 3 + 1;

    count++;

    evenOrOdd(number);
}
