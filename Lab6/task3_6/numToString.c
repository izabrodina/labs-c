#include <stdio.h>

#define N 100 // size of the string

void numberToString(int, char *);

int main()
{
    char string[N];
    int num;

    puts("Enter an integer:");
    while (!scanf("%d", &num))
    {
        puts("\nYou've mistyped the number.");
        return 1;
    }

    numberToString(num, string);

    printf("\nHere is the result string: \"%s\"\n", string);

    return 0;
}

void numberToString(int number, char *str)
{
    static int i = 0;

    if (number < 0)
    {
        str[i++] = '-';
        number = -number;
    }

    if (number / 10)
        numberToString(number / 10, str);

    str[i++] = (number % 10) + '0';
    str[i] = '\0';
}
