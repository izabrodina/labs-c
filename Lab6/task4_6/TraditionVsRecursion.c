#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <math.h>

void fillArray(int *, int);
void printArray(int *, int);
int traditionalWay(int *, int);
int recursiveWay(int *, int);


int main()
{
    int *arr, M, N;
    int sumTradition, sumRecursion;
    clock_t begin, end;
    double recTime, tradTime; // recTime - recursive way time,
                              // tradTime - traditional way time
    srand(time(0));

    puts("Enter the power of two:");
    scanf("%d", &M);

    N = pow(2, M);

    arr = (int *)malloc(sizeof(int)*N);
    if (arr == 0)
	{
		puts("Memory was not allocated");
        exit(1);
	}

    fillArray(arr, N);
    //printArray(arr, N);

    begin = clock();
    sumTradition = traditionalWay(arr, N);
    end = clock();
    printf("\nSum calculated in a traditional manner equals %d\n", sumTradition);
    tradTime = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Time: %f\n", tradTime);

    begin = clock();
    sumRecursion = recursiveWay(arr, N);
    end = clock();
    printf("\nSum calculated in a recursive manner equals %d\n", sumRecursion);
    recTime = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Time: %f\n", recTime);

    free(arr);

    return 0;
}

void fillArray(int *arr, int size)
{
    int i;

    for (i = 0; i < size; i++)
        arr[i] = rand()%(-100) + rand()%100;
}

void printArray(int *arr, int size)
{
    int i;

    putchar('\n');

    for (i = 0; i < size; i++)
        printf("%d ", arr[i]);
    putchar('\n');
}

int traditionalWay(int *arr, int size)
{
    int i, sum = 0;

    for (i = 0; i < size; i++)
        sum += arr[i];

    return sum;
}

int recursiveWay(int *arr, int size)
{
    if (size == 1)
        return arr[0];

    return recursiveWay(arr, size / 2) + recursiveWay(arr + size / 2, size - size / 2);
}
