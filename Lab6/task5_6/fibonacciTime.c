#include <stdio.h>
#include <time.h>

#define MIN 1
#define MAX 40

typedef unsigned long UL;

UL fibonacci(int num);

int main()
{
    int number;
    double time;
    clock_t begin, end;
    FILE *fp;

    fp = fopen("fibonacci.txt", "wt");

    if (!fp)
    {
        perror("File error");
        return 1;
    }

    printf("%s%13s%14s\n", "N-element", "Result", "Time");
    fprintf(fp, "%s%13s%14s\n", "N-element", "Result", "Time");

    for (number = MIN; number <= MAX; number++)
    {
        begin = clock();
        printf("%5d %16lu ", number, fibonacci(number));
        end = clock();
        time = (double)(end - begin) / CLOCKS_PER_SEC;
        printf("%15.5f\n", time);
        fprintf(fp, "%5d %16lu ", number, fibonacci(number));
        fprintf(fp, "%15.5f\n", time);
    }

    return 0;
}

UL fibonacci(int num)
{
    if (num == 1 || num == 2)
        return 1;

    else
        return fibonacci(num - 1) + fibonacci(num - 2);
}
