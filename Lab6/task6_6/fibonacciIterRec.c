#include <stdio.h>

#define MIN 1
#define MAX 50

typedef unsigned long long ULL;

ULL fibIter(ULL, ULL, int);
ULL fib(int);

int main()
{
    int i;

    printf("%s%10s\n", "#", "Result");

    for (i = MIN; i <= MAX; i++)
        printf("%-2d - %-15llu\n", i, fib(i));

    return 0;
}

ULL fib(int i)
{
    return fibIter(0, 1, i);
}

ULL fibIter(ULL L, ULL M, int N)
{
    if (N == 1)
        return M;

    else
        return fibIter(M, L + M, N - 1);
}
