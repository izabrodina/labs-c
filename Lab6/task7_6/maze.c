#include <stdio.h>
#include <windows.h>

#define N 9
#define M 28

char maze[N][M] = {
    "############################",
    "#           #   #          #",
    "##########  #   #          #",
    "#           #   #######  ###",
    "# ######    #              #",
    "#      #    #   #######   ##",
    "#####  #    #   #         ##",
    "       #        #     ######",
    "############################"};

void printMaze();
void wayOut(int, int);

int main()
{
    printMaze();
    wayOut(N / 2, M / 2);

    return 0;
}

void printMaze()
{
    int i, j;

    system("cls");

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
            putchar(maze[i][j]);
        putchar('\n');
    }
}

void wayOut(int row, int column)
{
    if (((column == 0 || column == M - 1) && maze[row][column] == ' ') ||
        ((row == 0 || row == N - 1) && maze[row][column] == ' '))
    {
        maze[row][column] = 'x';
        printMaze();
        puts("\nCongrats!");
        exit(1);
    }

    else if (maze[row][column] == ' ')
    {
        maze[row][column] = '.';
        printMaze();
        Sleep(50);
        wayOut(row - 1, column);
        wayOut(row + 1, column);
        wayOut(row, column - 1);
        wayOut(row, column + 1);
    }
}
