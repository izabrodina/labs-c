#include <stdio.h>

#define N 27
#define M 27

char picture[N][M];

void clearPicture();
int power(int);
void drawFractal(int, int, int);
void printFractal();

int main()
{
    clearPicture();
    drawFractal(N / 2, M / 2, 3);
    printFractal();

    return 0;
}

void clearPicture()
{
    int i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < M; j++)
            picture[i][j] = ' ';
}

int shift(int z)
{
    int i, value = 1;

    for (i = 0; i < z; i++)
        value *= 3;

    return value;
}

void drawFractal(int x, int y, int z)
{
    if (z == 0)
        picture[x][y] = '*';

    else
    {
        drawFractal(x, y, z - 1);
        drawFractal(x, y - shift(z - 1), z - 1);
        drawFractal(x, y + shift(z - 1), z - 1);
        drawFractal(x - shift(z - 1), y, z - 1);
        drawFractal(x + shift(z - 1), y, z - 1);
    }
}


void printFractal()
{
    int i, j;

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
            putchar(picture[i][j]);
        putchar('\n');
    }
}
