#include <stdio.h>

#define SIZE 63

int main()
{
    int i;
    char arr[SIZE] = "GFH734ghgRFu43fER3h";

    printf("Data in original order: ");

    for (i = 0; i <= SIZE - 1; i++ )
        printf("%c", arr[i]);

        putchar('\n');

    printf("Data in modified order: ");

            for (i = 0; i <= SIZE - 1; i++)
            {

                if (arr[i] >= 'A' && arr[i] <= 'Z')
                	printf("%c", arr[i]);
            }

            for (i = 0; i <= SIZE - 1; i++)
            {
                if(arr[i] >= 'a' && arr[i] <= 'z')
                    printf("%c", arr[i]);
            }

            for (i = 0; i <= SIZE - 1; i++)
            {
                if (arr[i] >= '0' && arr[i] <= '9')
                    printf("%c", arr[i]);
            }

    putchar('\n');

    return 0;
}
