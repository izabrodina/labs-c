#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int x, y, count = 0;

    srand(time(0));

    y = rand() % 100 + 1;

    while (x != y)
    {
        printf("Guess a number (from 1 to 100): ");
        scanf("%d", &x);
        count++;

            if (x > y)
            {
                printf("\nMy number is less than your guess. ");
                printf("Please try again\n\n");
                continue;
            }
            else if (x < y)
            {
                printf("\nMy number is bigger than your guess. ");
                printf("Please try again\n\n");
                continue;
            }
    }

    if (x == y)
        printf("\nThat's right! You win!\n");
        printf("You guessed right after %d tries!\n", count);

    return 0;
}
