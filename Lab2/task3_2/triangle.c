#include <stdio.h>

int main()
{
    int number, nline, i;  // number - total number of lines,
                           // nline - number of the exact line,
                           // i - index number of star position

    printf("Please enter the number of lines: ") ;
    scanf("%d", &number);

    for (nline = 0; nline < number; nline++)
    {
        for (i = 0; i < number - nline - 1; i++)
            putchar(' ');
        for (i = 0; i < nline * 2 + 1; i++)
            putchar('*');
        putchar('\n');
    }

    return 0;
}
