#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main ()
{
    int i, j, choice, length = 8;

    srand(time(0));

    for (j = 0; j < 10; j++)              // j - rows
        {
                for (i = 0; i < length; i++)  // i - column for each combination
                {
                    choice = rand() % 3;

                    switch (choice)
                    {
                    case 0:
                        putchar('a' + rand() % ('z' - 'a' + 1));
                        break;

                    case 1:
                        putchar('A' + rand() % ('Z' - 'A' + 1));
                        break;

                    case 2:
                        putchar('0' + rand() % ('9' - '0' + 1));
                        break;
                    }
                }

            printf ("\n");
        }

    return 0;
}
