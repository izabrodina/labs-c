#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#define G 9.81

int main()
{
    int t = 0;
    float H, h;

    printf("Please enter the height: ");
    scanf("%f", &H);

    printf("t = %d\th = %.1f\n", t++, H);

    while (h > 0)
    {
        h = H - G * t * t / 2;
        Sleep(1000);
        if (h > 0)
            printf("t = %d\th = %.1f\n", t++, h);
    }

    printf("\aBABAH!\n");

    return 0;
}
