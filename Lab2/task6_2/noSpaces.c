#include <stdio.h>
#include <string.h>

int main()
{
    char buf[256];
    int i = 0, z = 0;

    puts("Enter a string:");
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

    while (buf[i])
    {
        if (buf[0] == ' ')
        {
            for (z = 0; (buf[z]); z++)
                buf[z] = buf[z + 1];
        }
        if (buf[i] == ' ' && buf[i + 1] == ' ')
        {
            for (z = i; (buf[z]); z++)
                buf[z] = buf[z + 1];
                i--;
        }
        if (buf[i] == ' ' && buf[i + 1] == 0)
            buf[i] = 0;

        i++;
    }

    printf("\nModified string:\n\n%s\n", buf);

    return 0;
}
