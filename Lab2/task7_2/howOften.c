#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    char buf[256];
    int i = 0;
    int frequency[256] = {0};

    puts("Enter a string: ");
    fgets(buf, 256, stdin);
    buf[strlen(buf) - 1] = 0;

    printf("%7s%20s\n", "Letter:", "Frequency:");

    while (buf[i])
        {
            frequency[buf[i]]++;
            i++;
        }

    for (i = 0; i < 256; i++)
        {
            if (frequency[i])
                printf("%6c%20d\n", i, frequency[i]);
        }

    return 0;
}
