#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char buf[N];
    char *pWords[N] = {0};
    int i = 0, j = 0, count = 0, inWord = 0;

    puts("Enter a string:");
    fgets(buf, N, stdin);
    buf[strlen(buf) - 1] = ' ';

    while (buf[i])
    {
        if (buf[i] != ' ' && inWord == 0)
        {
            inWord = 1;
            pWords[count++] = &buf[i];
        }

        else if (buf[i] == ' ' && inWord == 1)
        {
            inWord = 0;
        }
        i++;
    }

    for (j = count - 1; j >= 0; j--)
    {
        while (*pWords[j] != ' ')
            putchar(*pWords[j]++);

        putchar(' ');
    }

    putchar('\n');

    return 0;
}
