#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char buf[N];
    char *pLeft, *pRight;
    int len;

    puts("Enter a string:");
    fgets(buf, N, stdin);
    len = strlen(buf);
    if (buf[len - 1] == '\n')
        buf[len-- - 1] = 0;

    pLeft = buf;
    pRight = buf + len - 1;

    while (pLeft < pRight)
    {
        if (*pLeft != *pRight)
            break;

        else
        {
            pLeft++;
            pRight--;
        }
    }

    if (pLeft >= pRight)
        printf("\nPalindrome\n");

    else
        printf("\nNot a palindrome\n");

    return 0;
}
