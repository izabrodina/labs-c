#include <stdio.h>
#include <string.h>

#define N 100
#define S 50

int main()
{
    char buf[S][N];
    char *young = {0}, *old = {0};
    int quantity, i = 0, age, maxAge = 0, minAge = 150;

    puts("Enter the number of relatives:");
    scanf("%d", &quantity);

    for (i = 0; i < quantity; i++)
    {
        printf("\nEnter the name of a relative: ");
        scanf("%s", buf[i]);
        printf("Enter the age of %s: ", buf[i]);

        if (!(scanf("%d", &age)) || age < 0 || age > 150)
        {
            puts("You've mistyped the age.");
            i--;
            continue;
        }

        else
        {
            if (maxAge < age)
            {
                maxAge = age;
                old = buf[i];
            }

            if (minAge > age)
            {
                minAge = age;
                young = buf[i];
            }
        }
    }

    printf("\nThe oldest relative is %s\n", old);
    printf("His/Her age is %d years\n\n", maxAge);
    printf("The youngest relative is %s\n", young);
    printf("His/Her age is %d years\n", minAge);

    return 0;
}
