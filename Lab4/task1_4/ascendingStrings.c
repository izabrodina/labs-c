#include <stdio.h>
#include <string.h>

#define N 100
#define S 10

void sort(char **, int);
void print(char **, int);

int main()
{
    char str[S][N];
    char *p[N];
    int i = 0, count = 0;

    puts("Enter strings (max 10 lines and 100 symbols in one line):");
    do
    {
        fgets(str[i], N, stdin);
        p[count] = str[i];
        i++;
        count++;
    }
    while (str[i - 1][0] != '\n' && i < S);

    sort(p, count);
    print(p, count);

    return 0;
}

void sort(char **buf, int counter)
{
    char *ptr;
    int i, j;

    for (i = counter - 1; i > 0; i--)
        for (j = 0; j < i; j++)
            if (strlen(buf[j]) > strlen(buf[j + 1]))
            {
                ptr = buf[j];
                buf[j] = buf[j + 1];
                buf[j + 1] = ptr;
            }
}

void print(char **buf, int counter)
{
    int i;

    for (i = 0; i < counter; i++)
        printf("%s", buf[i]);
}
