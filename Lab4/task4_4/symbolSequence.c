#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char str[N];
    char *p, *pMax;
    int i = 0, count = 0, maxCount = 0;

    puts("Enter a string:");
    fgets(str, N, stdin);

    str[strlen(str) - 1] = 0;

    while (str[i])
    {
        if (str[i] == str[i + 1])
        {
            count = 1;
            p = str + i;

            while (str[i] == str[i + 1])
            {
                count++;
                i++;
            }

            if (maxCount < count)
            {
                maxCount = count;
                pMax = p;
            }
        }

        i++;
    }

    if (maxCount <= 0)
        printf("\nA symbol sequence was not found.");

    else
    {
        printf("\nThe longest sequence of symbols: %d\n", maxCount);

        for (i = 0; i < maxCount; i++)
            putchar(*pMax);

    }

    putchar('\n');

    return 0;
}
