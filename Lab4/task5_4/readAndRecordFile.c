#include <stdio.h>
#include <string.h>

#define N 100
#define S 10

void sort(char **, int);

int main()
{
    char str[S][N];
    char *p[S] = {0};
    int i = 0, count = 0;
    FILE *fStart;
    FILE *fResult;

    fStart = fopen("strings.txt", "rt");
    fResult = fopen("resultStrings.txt", "wt");

    if (!fStart || !fResult)
    {
        perror("File error: ");
        return 1;
    }

    while (fgets(str[i], N, fStart) && count < S)
    {
        p[i] = str[i];
        count++;
        i++;
    }

    sort(p, count);

    for (i = 0; i < count; i++)
        fprintf(fResult, "%s", p[i]);

    fclose(fStart);
    fclose(fResult);

    return 0;
}

void sort(char **buf, int counter)
{
    char *ptr;
    int i, j;

    for (i = counter - 1; i > 0; i--)
        for (j = 0; j < i; j++)
            if (strlen(buf[j]) > strlen(buf[j + 1]))
            {
                ptr = buf[j];
                buf[j] = buf[j + 1];
                buf[j + 1] = ptr;
            }
}
