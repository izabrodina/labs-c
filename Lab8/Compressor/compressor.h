#ifndef _COMPRESSOR_
#define _COMPRESSOR_

struct SYM
{
	unsigned char ch;    // ASCII-code
	float freq;          // occurrence frequency
	char code[256];      // array for a new code
	struct SYM *left;    // left child
	struct SYM *right;   // right child
};

union CODE
{
	unsigned char ch;
	struct
	{
		unsigned short b1:1;
		unsigned short b2:1;
		unsigned short b3:1;
		unsigned short b4:1;
		unsigned short b5:1;
		unsigned short b6:1;
		unsigned short b7:1;
		unsigned short b8:1;
	}byte;
};

unsigned file_size_ext_name(char *file);
int analyser(char *file, struct SYM syms[], struct SYM *psyms[]);
struct SYM *buildTree(struct SYM *psyms[], int count, int new_count);
void makeCodes(struct SYM *root);
unsigned coder(char *file, struct SYM syms[], int count);
void pack_file(int count, struct SYM syms[], unsigned size_of_file, unsigned size_of_packed);
unsigned char pack(char buf[]);

#endif // _COMPRESSOR_
