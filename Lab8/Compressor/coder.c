#include "compressor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern char *file_name, *fname_coded;

unsigned coder(char *file, struct SYM syms[], int count)
{
	FILE *initial_file, *coded_file101;
	int ch, i;
	unsigned size = 0;

	if((initial_file=fopen(file, "rb")) == NULL)
	{
		puts("Input file open error.\n");
		exit(2);
	}//if

	fname_coded = (char *)calloc(strlen(file_name) + 4, sizeof(fname_coded));
	strcpy(fname_coded, file_name);
	strcat(fname_coded, ".101");

	if((coded_file101 = fopen(fname_coded, "wb")) == NULL)
	{
		puts("Coded file.101 create error.\n");
		exit(3);
	}//if

	ch = fgetc(initial_file);
	while(ch != EOF)
	{
		for(i = 0; i <= count; i++)
			if(syms[i].ch == (unsigned char)ch)
			{
				fputs(syms[i].code, coded_file101);
				size += strlen(syms[i].code);
				break;
			}//if

		ch = fgetc(initial_file);
	}//while

	if(size % 8)
	{
		for(i = 1; i <= 8 - (size % 8); i++)
			fputs("0", coded_file101);
	}//if

	fclose(initial_file);
	fclose(coded_file101);

	return size;
}//func file101_coding
