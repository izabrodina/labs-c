#include "compressor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct SYM *buildTree(struct SYM *psyms[], int count, int new_count)
{
	int i = 0;
	struct SYM *tmp_ptr;
	struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM));
	temp->freq = psyms[count - 1]->freq + psyms[count]->freq;
	temp->left = psyms[count];
	temp->right = psyms[count - 1];
	temp->code[0] = 0;

	if(count == 1)
		return temp;

	while(temp->freq < psyms[i]->freq)
		i++;
	new_count++;

	for(; i <= new_count; i++)
	{
		tmp_ptr = psyms[i];
		psyms[i] = temp;
		temp = tmp_ptr;
	}//for

	return buildTree(psyms, count - 1, new_count);
}//func buildTree

void makeCodes(struct SYM *root)
{
	if(root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}//if

	if(root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}//if
}//func makeCodes
