#include "compressor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern char *file_name, *file_ext, *fname_coded, *fname_packed;

void pack_file(int count, struct SYM syms[], unsigned size_of_file, unsigned size_of_packed)
{
	FILE *coded_file101, *packed_file;
	int i;
	unsigned char buf[8] = {0}, ch;

	if((coded_file101 = fopen(fname_coded, "rb")) == NULL)
	{
		puts("Coded file.101 opening error.\n");
		exit(4);
	}//if

	fname_packed = (char *)calloc(strlen(file_name) + 4, sizeof(fname_packed));
	strcpy(fname_packed, file_name);
	strcat(fname_packed, ".iri");

	if((packed_file = fopen(fname_packed, "wb")) == NULL)
	{
		puts("Packed file.iri creation error.\n");
		exit(5);
	}//if

	/*- HEADER -*/
	/*- Signature -*/
	fwrite("iri", sizeof(unsigned char), 3, packed_file);
	/*- Number of unique characters -*/
	fwrite(&count, sizeof(count), 1, packed_file);
	/*- Table of the occurrence -*/
	for(i = 0; i <= count; i++)
	{
		fwrite(&syms[i].ch, sizeof(unsigned char), 1, packed_file);
		fwrite(&syms[i].freq, sizeof(float), 1, packed_file);
	}//for
	/*- Tail length -*/
	if(size_of_packed % 8)
		i = 8 - (size_of_packed % 8);
	else
		i = 0;
	fwrite(&i, sizeof(int), 1, packed_file);
	/*- Initial file length -*/
	fwrite(&size_of_file, sizeof(unsigned), 1, packed_file);
	/*- Initial file extension -*/
	fwrite(file_ext, sizeof(char), 3, packed_file);
	/*- BODY -*/
	for(i = 0; i < size_of_packed; i += 8)
	{
		fread(buf, sizeof(unsigned char), 8, coded_file101);
		ch = pack(buf);
		fwrite(&ch, sizeof(unsigned char), 1, packed_file);
	}//while

	fclose(coded_file101);
	fclose(packed_file);
}//func pack_file

unsigned char pack(char buf[])
{
	union CODE code;
	code.byte.b1 = buf[0]-'0';
	code.byte.b2 = buf[1]-'0';
	code.byte.b3 = buf[2]-'0';
	code.byte.b4 = buf[3]-'0';
	code.byte.b5 = buf[4]-'0';
	code.byte.b6 = buf[5]-'0';
	code.byte.b7 = buf[6]-'0';
	code.byte.b8 = buf[7]-'0';
	return code.ch;
}//func pack
