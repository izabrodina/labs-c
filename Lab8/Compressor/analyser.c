#include "compressor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern char *file_name, *file_ext;

unsigned file_size_ext_name(char *file)
{
	FILE *initial_file;
	int i = 0;
	char *file_beg = file;
	unsigned size = 0;

	if((initial_file = fopen(file, "rb")) == NULL)
	{
		puts("Input file opening error.\n");
		exit(2);
	}//if

	fseek(initial_file, 0L, SEEK_END);
	size = ftell(initial_file);
	fclose(initial_file);

	while(*file++ != '.' && *file);

	if(*file)
	{
		file_name = (char *)calloc(strlen(file_beg) - strlen(file), sizeof(file_name));
		file_ext = (char *)calloc(strlen(file), sizeof(file_ext));

		while(*file_beg != '.')
			*(file_name + i++) = *file_beg++;

		i = 0;
		while(*file)
			*(file_ext + i++) = *file++;
	}//if

	return size;
}//func file_ext_and_name

int analyser(char *file, struct SYM syms[], struct SYM *psyms[])
{
	unsigned flag = 0, count = 0, symb_count = 0, i, j;
	int ch;
	struct SYM *tmp;
	FILE *initial_file;

	if((initial_file = fopen(file, "rb")) == NULL)
	{
		puts("Input file opening error.\n");
		exit(2);
	}//if

	ch = fgetc(initial_file);
	syms[0].ch = (unsigned char)ch;
	syms[0].freq = 0;
	syms[0].code[0] = 0;
	syms[0].left = syms[0].right = 0;
	psyms[0] = &syms[0];

	while(ch != EOF)
	{
		count++;
		for(i = 0; i <= symb_count; i++)
		{
			if(syms[i].ch == (unsigned char)ch)
			{
				syms[i].freq++;
				flag = 1;
			}//if
		}//for

		if(!flag)
		{
			syms[++symb_count].ch = (unsigned char)ch;
			syms[symb_count].freq = 1;
			syms[symb_count].code[0] = 0;
			syms[symb_count].left = syms[symb_count].right = 0;
			psyms[symb_count] = &syms[symb_count];
		}//if

		ch = fgetc(initial_file);
		flag = 0;
	}//while

	fclose(initial_file);

	for(i = 0; i <= symb_count; i++)
		syms[i].freq /= count;

	for(i = 0; i <= symb_count; i++)
	{
		for(j = symb_count; j > i; j--)
		{
			if(psyms[j - 1]->freq < psyms[j]->freq)
			{
				tmp = psyms[j - 1];
				psyms[j - 1] = psyms[j];
				psyms[j] = tmp;
			}//if
		}//for
	}//for

	return symb_count;
}//func analyser
