#include "compressor.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *file_name, *file_ext, *fname_coded, *fname_packed;

int main(int argc, char *argv[])
{
	struct SYM syms[256], *psyms[512], *root;
	int count;
	unsigned size_of_packed = 0, size_of_file = 0;

	if(argc != 2)
	{
		puts("Incorrect syntax.\nSyntax: compressor.exe file.xxx\n");
		exit(1);
	}//if

	size_of_file = file_size_ext_name(argv[1]);
    count = analyser(argv[1], syms, psyms);
    root = buildTree(psyms, count, count);
    makeCodes(root);
    size_of_packed = coder(argv[1], syms, count);
    pack_file(count, syms, size_of_file, size_of_packed);

	return 0;
}//main
