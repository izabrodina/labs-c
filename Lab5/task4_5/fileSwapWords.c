#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 256

void cut(char *);
int getWords(char *, char **);
void printWord(char *);

FILE *fStart, *fResult;

int main()
{
    char string[SIZE];
    char *word[SIZE];
    int count, number, n;

    fStart = fopen("1.txt", "rt");
    fResult = fopen("2.txt", "wt");

    if (!fStart || !fResult)
    {
        perror("File error");
        exit(1);
    }

    srand(time(0));

    while (fgets(string, SIZE, fStart))
    {
        cut(string);
        count = number = getWords(string, word);

        while (count)
        {
            n = rand() % number;

            if (word[n])
            {
                printWord(word[n]);
                word[n] = 0;
                count--;
            }
        }

    fputc('\n', fResult);
    }

    fclose(fStart);
    fclose(fResult);

    return 0;
}

void cut(char *str)
{
    if (str[strlen(str) - 1] == '\n')
        str[strlen(str) - 1] = 0;
}
int getWords(char *str, char **ptr)
{
    int count = 0, inWord = 0;

    while (*str)
    {
        if (*str != ' ' && inWord == 0)
        {
            inWord = 1;
            ptr[count++] = str;
        }

        else if (*str == ' ' && inWord == 1)
            inWord = 0;

        str++;
    }

    return count;
}

void printWord(char *str)
{
    while (*str != ' ' && *str)
        fputc(*str++, fResult);
    fputc(' ', fResult);
}
