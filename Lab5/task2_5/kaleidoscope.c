#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#define N 20
#define M 20
#define DENSITY 35

void clearScreen(char (*str)[M]);
void fillUpperLeft(char (*str)[M]);
void copyPattern(char (*str)[M]);
void printPattern(char (*str)[M]);

int main()
{
    char buf[N][M];

    while (1)
    {
        system("cls");
        srand(time(0));
        clearScreen(buf);
        fillUpperLeft(buf);
        copyPattern(buf);
        printPattern(buf);
        Sleep(1500);
    }

    return 0;
}

void clearScreen(char (*str)[M])
{
    int i, j;

    for (i = 0; i < N; i++)
        for (j = 0; j < M; j++)
            str[i][j] = ' ';
}

void fillUpperLeft(char (*str)[M])
{
    int i, j;
    int count = DENSITY;

    while (count > 0)
    {
        i = rand() % (N / 2);
        j = rand() % (M / 2);

        if (str[i][j] == ' ')
        {
            str[i][j] = '*';
            count--;
        }
    }
}

void copyPattern(char (*str)[M])
{
    int i, j;

    for (i = 0; i < N; i++)
        for (j = 0; j < M; j++)
        {
            if (str[i][j] == '*')
            {
                str[N - i - 1][j] = '*';
                str[N - i - 1][M - j - 1] = '*';
                str[i][M - j - 1] = '*';
            }
        }
}

void printPattern(char (*str)[M])
{
    int i, j;

    for (i = 0; i < N; i++)
        for (j = 0; j < M; j++)
        {
            putchar(str[i][j]);

            if (j == M - 1)
                putchar('\n');
        }

    putchar('\n');
}
