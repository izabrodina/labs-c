#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAX 256

void cut(char* buf);
int getWords(char *buf, char **p);
void randSwap(char **word);
void swapSymbols(char *a, char *b);

int main()
{
    int i, count;
    char string[MAX];
    char *wordBegin[MAX] = {0};

    FILE *fStart, *fResult;

    fStart = fopen("harryPotter.txt", "rt");
    fResult = fopen("result.txt", "wt");

    if (!fStart || !fResult)
    {
        perror("File error");
        exit(1);
    }

    while (fgets(string, MAX, fStart))
    {
        cut(string);
        count = getWords(string, wordBegin);

        for (i = 0; i < count; i++)
            randSwap(&wordBegin[i]);

            fprintf(fResult, "%s\n", string);
    }

    fclose(fStart);
    fclose(fResult);

    return 0;
}

void cut(char* buf)
{
    if (buf[strlen(buf) - 1] == '\n')
        buf[strlen(buf) - 1] = 0;
}

int getWords(char *buf, char **p)
{
    int inWord = 0, count = 0;

    while (*buf)
    {
        if (*buf != ' ' && inWord == 0)
        {
            p[count++] = buf;
            inWord = 1;
        }

        else if (*buf == ' ')
            inWord = 0;

        buf++;
    }

    return count;
}

void swapSymbols(char *a, char *b)
{
    char temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void randSwap(char **word)
{
    int i = 0, position = 1;
    char *a, *b;

    srand(time(0));

    while (*(*word + i) != ' ' && *(*word + i) && *(*word + i) != ',' && *(*word + i) != '.' && *(*word + i) != '!' && *(*word + i) != '?' && *(*word + i) != '"')
        i++;

    if (i < 3)
        return;

    while (i > (position + 2))
    {
        a = *word + position;
        b = *word + (position + 1) + rand() % (i - 2 - position);
        swapSymbols(a, b);
        position++;
    }
}
