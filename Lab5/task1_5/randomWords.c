#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 256

int getWords(char *, char **);
void printWord(char *);

int main()
{
    char buf[N];
    char *p[N] = {0};
    int counter, number, n;

    srand(time(0));

    puts("Enter a string:");
    fgets(buf, N, stdin);
    buf[strlen(buf) - 1] = ' ';

    counter = number = getWords(buf, p);

    printf("\nModified string:\n");

    while (counter)
    {
        n = rand() % number;

        if (p[n])
        {
            printWord(p[n]);
            putchar(' ');
            p[n] = 0;
            counter--;
        }
    }

    putchar('\n');

    return 0;
}

int getWords(char *str, char **ptr)
{
    int count = 0, inWord = 0;

    while (*str)
    {
        if (*str != ' ' && inWord == 0)
        {
            inWord = 1;
            ptr[count++] = str;
        }

        else if (*str == ' ' && inWord == 1)
            inWord = 0;

        str++;
    }

    return count;
}

void printWord(char *str)
{
    while (*str != ' ' && *str)
        putchar(*str++);
}
