#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct WORD
{
    char word[256];
    unsigned int count;
    struct WORD *left;
    struct WORD *right;
};

char *chomp(char *str);
char *cleanBuf(char *buf, int wordLen);
struct WORD *makeTree(struct WORD *words, char *add); // to add arguments to the tree
void searchTree(struct WORD *words, char *buf);
void printTree(struct WORD *words);

int main(int argc, char *argv[])
{
    FILE *fp_keyWords, *fp_sourceFile;
    char buf[256] = {0}, symbol;
    int i = 0;
    struct WORD *words = 0;

    if (argc != 3)
    {
        puts("Incorrect amount of arguments.");
        puts("Usage: keyWords.exe keyWords.txt sourceFile.txt.");
        exit(1);
    }
    fp_keyWords = fopen(argv[1], "rt");
    fp_sourceFile = fopen(argv[2], "rt");

    if (!fp_keyWords || !fp_sourceFile)
    {
        perror("File error");
        exit(2);
    }

    while (!feof(fp_keyWords))  // while EOF is not reached
    {
        fscanf(fp_keyWords, "%s", buf);
        chomp(buf);
        words = makeTree(words, buf);
        cleanBuf(buf, strlen(buf));
    }

    fclose(fp_keyWords);

    while (!feof(fp_sourceFile))  // while EOF is not reached
    {
        symbol = fgetc(fp_sourceFile);

        if (symbol >= 'a' && symbol <= 'z')
        {
            buf[i++] = symbol;
        }

        else
        {
            if (i) // if there is a word
            {
                searchTree(words, buf);
                i = 0;
                cleanBuf(buf, strlen(buf));
            }
        }
    }

    fclose(fp_sourceFile);

    puts("\nKey Words Occurrence Table:\n");
    printTree(words);

    return 0;
}

char *chomp(char *str)
{
    if(str[strlen(str) - 1] == '\n')
       str[strlen(str) - 1] = 0;

    return str;
}

char *cleanBuf(char *buf, int wordLen)
{
    int i;

    for (i = 0; i < wordLen; i++)
        buf[i] = 0;

    return buf;
}

struct WORD *makeTree(struct WORD *words, char *add)
{
    if (words == NULL) // if there were no words
    {
        words = (struct WORD *)malloc(sizeof(struct WORD));
        strcpy(words->word, add);
        words->count = 0;
        words->left = words->right = 0;
    }

    else if (strcmp(words->word, add) > 0)
        words->left = makeTree(words->left, add);

    else if (strcmp(words->word, add) < 0)
        words->right = makeTree(words->right, add);

    return words;
}

void searchTree(struct WORD *words, char *buf)
{
    if (words == NULL)
        return;

    else if (!strcmp(words->word, buf))
        words->count++;

    else if (strcmp(words->word, buf) > 0)
        searchTree(words->left, buf);

    else if (strcmp(words->word, buf) < 0)
        searchTree(words->right, buf);
}

void printTree(struct WORD *words)
{
    if (strlen(words->word))
        printf("%s - %d\n", words->word, words->count);
    if (words->left)
        printTree(words->left);
    if (words->right)
        printTree(words->right);
}
