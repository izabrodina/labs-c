#include <stdio.h>
#include <stdlib.h>

struct NODE
{
    unsigned char sym;
    unsigned long freq; // occurrence frequency
    struct NODE *left;
    struct NODE *right;
};
typedef struct NODE TNODE;
typedef TNODE* PNODE;

PNODE createTree(PNODE root, char sym);
unsigned long findMax(PNODE root);
void printTree(PNODE root, unsigned long max);

int main(int argc, char *argv[])
{
    FILE *fp;
    int ch;
    PNODE root = NULL;
    unsigned long max;

    fp = fopen(argv[1], "rt");
    if (!fp)
    {
        perror("Error");
        exit(1);
    }

    while ((ch = fgetc(fp)) != EOF)
        root = createTree(root, (char)ch);

    max = findMax(root);
    printf("\nMax = %lu\n", max);

    puts("\nSymbol Occurrence Frequency:\n");

    for (; max > 0; max--)
    {
        printTree(root, max);
    }

    fclose(fp);

    return 0;
}

PNODE createTree(PNODE root, char sym)
{
    if (root == NULL)
    {
        root = (PNODE)malloc(sizeof(TNODE));
        root->sym = sym;
        root->freq = 1;
        root->left = root->right = NULL;
    }

    else if (root->sym > sym)
        root->left = createTree(root->left, sym);

    else if (root->sym < sym)
        root->right = createTree(root->right, sym);

    else
        root->freq++;

    return root;
}

unsigned long findMax(PNODE root)
{
    static unsigned long max = 0;

    if (root->left)
    {
        findMax(root->left);
    }

    if (root->right)
    {
        findMax(root->right);
    }

    if ((root->freq) > max)
    {
        max = root->freq;
    }

    return max;
}

void printTree(PNODE root, unsigned long max)
{
    char ch;

    if (root->left)
    {
        printTree(root->left, max);
    }

    if (root->right)
    {
        printTree(root->right, max);
    }

    if ((root->freq) == max && root->freq)
    {
        max = root->freq;
        ch = root->sym;
        printf("%c - %lu\n", ch, max);
        root->freq = 0;
    }
}
