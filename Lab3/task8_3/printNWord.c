#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char str[N];
    int n, i = 0, count = 0, inWord = 0;

    puts("Enter a string:");
    fgets(str, N, stdin);
    str[strlen(str) - 1] = 0;

    puts("Enter a word number:");
    scanf("%d", &n);

    while (str[i])
    {
        if (str[i] != ' ' && inWord == 0)
        {
            inWord = 1;
            count++;
        }

        else if (str[i] == ' ' && inWord == 1)
            inWord = 0;

        if (count == n && str[i] != ' ')
            putchar(str[i]);

        i++;
    }

    putchar('\n');

    if (n <= 0 || n > count)
        puts("Error! You've typed a wrong number");

    return 0;
}
