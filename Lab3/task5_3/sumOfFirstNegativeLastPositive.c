#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 10

int main()
{
    int arr[N];
    int i = 0, firstNegative, lastPositive, sum = 0;

    srand(time(0));

    for (i = 0; i < N; i++)
    {
        arr[i] = (rand() - RAND_MAX / 2) % 100;
        printf("%4d", arr[i]);
    }

    for (i = 0; i < N; i++)
        if (arr[i] < 0)
        {
            firstNegative = i;
            break;
        }
    printf("\n\nFirst negative number: %d\n", arr[firstNegative]);

    for (i = N - 1; i >= 0; i--)
        if (arr[i] > 0)
        {
            lastPositive = i;
            break;
        }

    printf("Last positive number: %d", arr[lastPositive]);

    while (firstNegative < lastPositive - 1)
    {
        sum += arr[firstNegative + 1];
        firstNegative++;
    }

    printf("\n\nSum is: %d\n", sum);

    return 0;
}
