#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char string[N];
    int inWord = 0, count = 0, len, i = 0;

    puts("Enter a string:\n");
    fgets(string, N, stdin);

    len = strlen(string);
    if (string[len - 1] == '\n')
        string[len-- - 1] = 0;

    while (string[i])
    {
        if (string[i] != ' ' && inWord == 0)
        {
            inWord = 1;
            count++;
        }

        else if (string[i] == ' ' && inWord == 1)
            inWord = 0;
        i++;
    }

    printf("\nWords number: %d\n", count);

    return 0;
}
