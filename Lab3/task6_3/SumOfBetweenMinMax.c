#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 10

int main()
{
    int arr[N];
    int i = 0, min = 0, max = 0, sum = 0;  // min - initial position of minimum number
                                           // max - initial position of maximum number
    srand(time(0));

    for (i = 0; i < N; i++)
    {
        arr[i] = (rand() - RAND_MAX / 2) % 100;
        printf("%4d", arr[i]);
    }

    for (i = 0; i < N; i++)
    {
        if (arr[i] > arr[max])
            max = i;
        else if (arr[i] < arr[min])
            min = i;
    }
    printf("\n\nMaximum number: %d\n", arr[max]);
    printf("Minimum number: %d", arr[min]);

    if (max > min)
    {
        for (i = min + 1; i < max; i++)
            sum += arr[i];
    }

    else
    {
        for (i = max + 1; i < min; i++)
            sum += arr[i];
    }

    printf("\n\nSum is: %d\n", sum);

    return 0;
}
