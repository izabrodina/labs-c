#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char str[N];
    int frequency[N] = {0};
    int i = 0, z, max, position; // max - the max value of symbols counter
                                 // position - max position in the string
                                 // z - symbols counter for different symbols appeared in a string

    puts("Enter a string:");
    fgets(str, N, stdin);
    str[strlen(str) - 1] = 0;

    while (str[i])              // filling the frequency array
    {
        ++frequency[str[i]];
        i++;
    }

    printf("%2s%15s\n\n", "Symbol", "Frequency");

    for (i = 0, z = 0; i < N; i++)
    {
        if (frequency[i] > 0)
            z++;
    }

    while (z > 0)
    {
        for (max = 0, i = 0; i < N; i++) // finding the max counter value
        {
            if (frequency[i] > max)
            {
                max = frequency[i];
                position = i;
            }
        }

        printf("%4c%13d\n", position, max);
        frequency[position] = 0;        // getting rid of the max value
                                        // for finding the next one
        z--;
    }

    return 0;
}
