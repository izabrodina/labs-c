#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char str[N];
    int i = 0, count = 0, maxCount = 0, startPosition = 0, start = 0;

    puts("Enter a string:");
    fgets(str, N, stdin);

    str[strlen(str) - 1] = 0;

    while (str[i])
    {
        if (str[i] == str[i + 1])
        {
            count++;
            start = 1;
        }

        else if (str[i] != str[i + 1] && start == 1)
        {
            count++;

            if (maxCount < count)
            {
                maxCount = count;
                startPosition = i - maxCount + 1;
            }
            count = 0;
            start = 0;
        }

        i++;
    }

    printf("\nThe longest sequence of symbols: %d\n", maxCount);

    for (i = startPosition; i < startPosition + maxCount; i++)
        putchar(str[i]);

    putchar('\n');

    return 0;
}
