#include <stdio.h>
#include <string.h>

#define S 256

int main()
{
    char str[S];
    int n, i = 0, count = 0, len, inWord = 0, start = 0, end = 0;

    puts("Enter a string:");
    fgets(str, S, stdin);
    len = strlen(str);
    str[len - 1] = ' ';

    puts("\nEnter a word number:");
    scanf("%d", &n);

    putchar('\n');

    while (str[i])
    {
        if (str[i] != ' ' && inWord == 0)
        {
            inWord = 1;
            count++;

            if (n == count)
                start = i;
        }

        else if (str[i] == ' ' && inWord == 1)
        {
            if (n == count)
                end = i;
            inWord = 0;
        }
        i++;
    }

    if (n <= count && n > 0)
    {
        for (i = 0; i < start; i++)
            putchar(str[i]);

        for (i = end + 1; i < len; i++)
            putchar(str[i]);

        putchar('\n');
    }

    else if (n <= 0 || n > count)
        puts("\nError. You've typed a wrong number\n");

    return 0;
}
