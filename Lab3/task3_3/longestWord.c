#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char buf[N];
    int inWord = 0, len, wordLen = 0, maxLen = 0;
    int i = 0, j = 0;            // j - the beginning of the longest word

    puts("Please enter a string:\n");
    fgets(buf, N, stdin);

    len = strlen(buf);
    if (buf[len - 1] == '\n')
        buf[len - 1] = ' ';

    while (buf[i])
    {
        if (buf[i] != ' ' && inWord == 0)
            {
                inWord = 1;
                wordLen = 1;
            }

        else if (buf[i] != ' ' && inWord == 1)
            wordLen++;

        else if (buf[i] == ' ' && inWord == 1)
        {
            if (wordLen > maxLen)
            {
                maxLen = wordLen;
                j = i - maxLen;
            }
            inWord = 0;
            wordLen = 0;
        }
        i++;
    }

    printf("\nThe longest word is ");

    while (buf[j] != ' ')
    {
        putchar(buf[j]);
        j++;
    }

    printf("\nIts length: %d\n", maxLen);

    return 0;
}
