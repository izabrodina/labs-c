#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
	char buf[N];
    int inWord = 0, len, wordLen = 0, count = 0, i = 0;

	puts("Enter a string:\n");
	fgets(buf, N, stdin);
	putchar('\n');

	len = strlen(buf);
    if (buf[len - 1] == '\n')
        buf[len-- - 1] = 0;

	while (buf[i])
	{
		if (buf[i] != ' ')
			putchar(buf[i]);

		if (buf[i] != ' ' && inWord == 0)
		{
			inWord=1;
			wordLen = 1;
			count++;
		}

		else if (buf[i] == ' ' && inWord == 1)
		{
			inWord = 0;
			printf(" %15s %d\n", "-   Word length:", wordLen);
			wordLen = 0;
		}
		else if (inWord == 1)
            wordLen++;

		i++;
	}

	printf(" %15s %d\n", "-   Word length:", wordLen);
	printf("\nWords number: %d\n", count);

	return 0;
}
