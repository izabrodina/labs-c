#include <stdio.h>
#include <string.h>

#define N 256

int main()
{
    char str[N];
    int i = 0, count, sum = 0, totalNumber;

    puts("Enter a string:");
    fgets(str, N, stdin);

    str[strlen(str) - 1] = 0;

    while (str[i])
    {
        if (str[i] >= '0' && str[i] <= '9')
        {
            count = 0, totalNumber = 0;

            while (str[i] >= '0' && str[i] <= '9' && count < 3)
            {
                totalNumber *= 10;
                totalNumber += str[i] - '0';
                count++;
                i++;
            }

            sum += totalNumber;
        }

        else
            i++;
    }

    printf("\nThe sum of the numbers is: %d\n", sum);

    return 0;
}
