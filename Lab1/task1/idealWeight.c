#include <stdio.h>

int main()
{
    char name[50];
    int gender, height, weight, idealWeight;

    puts("Good day! What is your name?");
    scanf("%s",name);
    printf("%s, what is your gender (Male - 1/Female - 2)? ", name);
    scanf("%d", &gender);
    printf("\nPlease enter your height in centimeters: ");
    scanf("%d", &height);
    printf("Please enter your weight in kilograms: ");
    scanf("%d", &weight);


    if (gender == 1)
        idealWeight = (height - 100) * 0.9;
    else
        idealWeight = (height - 100) * 0.85;


    if(weight <= idealWeight + 5 && weight >= idealWeight - 5)
        printf("\nWell, %s, your weight is normal.\n\nCongratulations!\n", name);

    else if(weight < idealWeight - 5)
        printf("\nWell, %s, your weight is below normal.\n\nIt is recommended for you to gain weight.\n", name);

    else
        printf("\nWell, %s, your weight is above normal.\n\nIt is recommended for you to lose weight.\n", name);

    return 0;
}
