#include <stdio.h>
#define I 12
#define S 2.54

int main()
{
    int feet;
    float inch, cm, meter;

    printf("Please enter your height in feet and inches (example: 5 feet 4.96 inches):\n");
    scanf("%d%*s%f%*s", &feet, &inch);

    cm = feet * I * S + inch * S;
    meter = cm / 100;

    printf("\nYour height in centimeters is %.fcm\n\n", cm);
    printf("Your height in meters is %.2fm\n", meter);

    return 0;
}
