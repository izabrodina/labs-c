#include <stdio.h>

int main()
{
    char name[50];
    int hour, minute, second;

    printf("Please enter your name: ");
    scanf("%s", name);
    printf("\n%s, what time is it?\n", name);
    printf("Please enter the time in the following format HH:MM:SS (24-hour clock):\n");
    scanf("%d:%d:%d", &hour, &minute, &second);

    if (hour >= 0 && hour < 24 && minute >= 0 && minute < 60 && second >=0 && second < 60)
        {
            if(hour >= 5 && hour < 12)
                printf("\nGood Morning, %s!\n", name);

            else if (hour >= 12 && hour < 17)
                printf("\nGood Afternoon, %s!\n", name);

            else if(hour >= 17 && hour < 22)
                printf("\nGood Evening, %s!\n", name);

            else
                printf("\nGood Night, %s!\n", name);
        }

    else
            printf("You've entered invalid time.\n");

    return 0;
}
