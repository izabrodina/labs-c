#include <stdio.h>
#define D 0.01745
#define R 57.29578

int main()
{
    float number;
    char letter;

    printf("Please enter the value in radians or degrees (00.00R or 00.00D):\n");
    scanf("%f%c", &number, &letter);

    if(letter == 'R' || letter == 'r')
        printf("\n%.2f%c = %.2fD\n", number, letter, number * R);

    else if(letter == 'D' || letter == 'd')
        printf("\n%.2f%c = %.2fR\n", number, letter, number * D);

    else
        printf("You've entered a wrong letter.\n");

    return 0;
}
