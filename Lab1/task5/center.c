#include <stdio.h>
#include <string.h>

int main()
{
    char string[52];
    int i, max = 52;

    printf("Please enter a string (not longer than 50 symbols):\n");
    fgets(string, 52, stdin);

    int len = strlen(string);
    int space = (max - len) / 2;

    for (i = 0; i < space; i++)
        printf(" ");

    printf("%s", string);

    for (i = space + len; i <= max; i++)
        printf(" ");

    return 0;
}
